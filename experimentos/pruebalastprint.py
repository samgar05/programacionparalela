from multiprocessing import Process, Semaphore
import time

def trabajar(sem, tr_id) -> None:
    sem.acquire()
    print(f'{tr_id} esta trabajando', flush = True)
    time.sleep(0.5)
    print(f'{tr_id} ha acabado', flush = True)
    sem.release()
    
if __name__ == '__main__':
    semaforo = Semaphore(2)
    
    procesos = []
    for i in range(5):
        p = Process(target = trabajar, args= (semaforo, i))
        procesos.append(p)
        p.start()
    
    for p in procesos:
        p.join()
    
    print("Todo listo")
        
    