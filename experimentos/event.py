from multiprocessing import Process, Event

def tarea1(event):
    event.set()
    print('ya!')
    
def tarea2(event):
    event.wait()
    print('oido')
    event.wait()
    print('oido2')
    #event.clear()
    #print('olvidado')
    event.wait()
    print('oido3')
    
if __name__ == '__main__':
    global e
    e = Event()
    
    p1 = Process(target = tarea1, args= (e,))
    p2 = Process(target = tarea2, args= (e,))
    
    p1.start()
    p2.start()
    
    p1.join()
    p2.join()
    
    print('Terminado')