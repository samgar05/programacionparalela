import multiprocessing
import math

def es_primo(n:int) -> bool:
    if n < 2:
        return False
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

def final(n:int) -> bool:
    return n%1_000==227
    
    f
    
    
    
    

def buscar_numero(condicion, cola_archivos, evento):
    while not evento.is_set() and not cola_archivos.empty():
        nombre_archivo = cola_archivos.get()
        with open(nombre_archivo, 'r') as archivo:
            for linea in archivo:
                numero = int(linea.strip())
                if condicion(numero):
                    evento.set()
                    print(f"¡Número encontrado! {numero} en el archivo {nombre_archivo}")
                    break

if __name__ == '__main__':
    # Colocar los nombres de los archivos en la cola
    archivos = ['datos0', 'datos1','datos2','datos3', 'datos4']
    cola_archivos = multiprocessing.Queue()
    for archivo in archivos:
        cola_archivos.put(archivo)

    # Crear evento para coordinar los procesos
    evento_encontrado = multiprocessing.Event()

    # Crear procesos buscadores
    nr_buscadores = 4
    procesos = []
    for _ in range(nr_buscadores):
        proceso = multiprocessing.Process(target=buscar_numero, args=(es_primo, cola_archivos, evento_encontrado))
        procesos.append(proceso)
        proceso.start()

    # Esperar a que uno de los procesos encuentre un número o todos terminen
    for proceso in procesos:
        proceso.join()

    # Imprimir mensaje si ningún proceso encontró un número
    if not evento_encontrado.is_set():
        print("Ningún número encontrado en los archivos.")