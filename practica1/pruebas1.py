import math


datos0 = open('datos0')

listaNums = list(map(int,datos0.read().split(' ')))

def es_primo(n:int) -> bool:
    if n < 2:
        return False
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

def acabaEn227(n:int) -> bool:
    return n % 1000 == 227

def allCondition(fileList:list,condicion:callable) -> None:
    
    for filename in fileList:
        datosi = open(filename)
        listaNums = list(map(int,datosi.read().split(' ')))
        numero_de_archivo = filename[len(filename)-1]
        
        contadorPrimos = 0
        for num in listaNums:
            if condicion(num):
                contadorPrimos += 1
                print('El numero {} cumple la condicion'.format(num))
                if es_primo(num):
                    print('Además es primo')
                else:
                    print('Pero no es primo')
        print('\nEn total hay {} numeros que cumplen {} en el archivo {}\n'.format(contadorPrimos, condicion.__name__, numero_de_archivo))



#allCondition(['datos0', 'datos1','datos2','datos3','datos4'], es_primo)
allCondition(['datos0', 'datos1','datos2','datos3','datos4'], acabaEn227)
