## Entregable 1

## Integrantes: - García Herrera, Samuel Fernando
##              - Llantoy Salvatierra, Ostin Antenor 
##              - Ruiz Bacete, Lucas  



from multiprocessing import Process, Event, Queue
from math import sqrt



def condicion(n:int) -> bool: # condición cualquiera en verdad
    if n < 2:
        return False
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False

    if n%1000==227:
        return True
    else:
        return False


# La siguiente función despliega los valores de una cola dada la misma cola
# pero, aplicado al caso del ejercicio, se muestran los nombres de los
# archivos donde se han encontrado los numeros que cumplen la condicion.
def desplegarValoresCola(q:Queue())->None: 
    i = 1
    while not q.empty():
        a,b = q.get()
        print(str(i)+". Archivo de texto: "+a+" => numero encontrado: "+str(b)+"\n")
        i+=1
              

# Esta función -buscarEnCola- es la que se le manda a cada proceso y lo que hace 
# es coger elementos de la cola e ir trabajando sobre ellos de modo
# que se revise si en cada archivo hay un elemento que cumpla la 
# condición. Si la cumple va acumulando los números obtenidos en una
# cola y llama a una función gestor para que controle si la cantidad 
# acumulada de numeros que cumplen la condición es la adecuada. 
def buscarEnCola(gest:callable,cond:callable,q:Queue(),q2:Queue(),e:Event(),n:int)->None:  
    
    corta = True
    
    while not q.empty() and corta==True: 
        
        dato=q.get() # se coge un elemento de la cola donde se almacenan todos los nombres de los archivos a procesar
        
        auxTexto = open(dato,"r").read() # se procesa como tal el archivo tomado
        
        lista = auxTexto.split() # esta es lista de caracteres de enteros dado el archivo a analizar
        
        for a in lista: # se analizan los valores del archivo: si cumple la condicion o no y, si la cumple, llama a una función que gestiona la acumulación de valores que satisfacen la condición    
            
            entero = int(a)
            
            if cond(entero) and e.is_set(): # (0) Ver abajo -función gestor-.
                
                q2.put((dato,entero))  # zona conflictiva: puede ocurrir que dado un proceso A que induzca una inhabilitación por medio de
                
                gestor(q, q2, e, n)    # zona conflictiva: la función gestor otro proceso B coloque, antes de dicha inhabilitación, otro 
                
            elif not e.is_set():       # zona conflictiva: numero valido a la cola, más de los necesarios. Esto se explica mejor abajo. 
            
                corta=False


def gestor(q0:Queue(),q:Queue(),e:Event(),n:int)->None: 
    
    # En principio esta función estaba ideada para que la función
    # gestor solo inhabilitara el evento cuando la cola donde se 
    # recogen los valores que cumplen la condición (qRepositorio) 
    # tuviese una cantidad de valores igual a la cantidad requerida de
    # estos valores. Bajo esta premisa, la función quedaría tal cual 
    # es ahora salvo por la definición del condicional en (1), 
    # puesto que tendría que ser una igualdad y no desigualdad.
    # Dicha desigualdad, por ser igual o mayor, también contempla
    # el caso ideal con igualdad. 
    # Pero dicha desigualdad atiende también a un caso que puede pasar 
    # debido a la independencia de ejecución de las instrucciones 
    # por los procesos. 
    # Puede ocurrir que dado un proceso A que esté llamando a la función
    # gestor, al mismo tiempo que empieza a ejecutar dicha 
    # función gestor, otro proceso B ya haya metido un elemento más 
    # a la cola - uno más de los requeridos - pues en dicho tiempo 
    # aún no se habría inhabilitado el evento por el proceso A, 
    # llevando al proceso B a satisfacer la primera condicional 
    # en (0) y añadiendo un elemento más a la cola. De no poner esta 
    # desigualdad y quedarse 
    # con la igualdad podría no satisfacerse la condicional en (1)
    # para otros procesos, haciendo que estos no se paren por el bloqueo 
    # que supone la inhabilitación del evento 
    # , debido a la cantidad de datos ya almacenados por dicha 
    # 'maligna' concurrencia de datos, y hacer que los procesos
    # sigan buscando datos aún habiendo ya más de los requeridos.
    # Con el tiempo, ya no habrá más datos que procesar pero entonces
    # la cola qRepositorio tendrá todos los elementos que cumplen 
    # la condición de todos los archivos, pero esto no es 
    # precisamente lo que se quiere. Poniendo una desigualdad   
    # aún hay inconvenientes al respecto del almacenaje de datos.
    # No solo puede ocurrir con el proceso B lo visto antes, esto
    # mismo podría ocurrir a varios procesos y hacer que se llene
    # la cola más de lo debido en ese lapso de tiempo comentado.
    # Pero posteriormente cuando se haya inhabilitado el 
    # evento -asegurado por la desigualdad, al contrario que con la igualdad- 
    # ya no se podrán añadir más elementos a la cola qRepositorio y se 
    # suspenderá la ejecución de los demás procesos. Para 
    # entonces puede que en la cola qRepositorio halla más datos 
    # de los requeridos pero serán, seguro, muchos menos  
    # que de haberlos procesado del total de archivos que se tiene -
    # lo que podría pasar con el caso de la igualdad-.
    
    # Esta situación, de todos modos, es bastante hipotética pues 
    # los resultados obtenidos de la ejecución del código no han 
    # reportado dichos casos maliciosos donde la cantidad de valores
    # obtenidos es mayor que la solicitada. No obstante, es un
    # caso contemplable y por eso se pone la desigualdad. 
    
    if q.qsize()>=n: # (1)   
        
       e.clear()
       
       print("Se ha obtenido la cantidad de numeros solicitados.\n", flush=True)
       
       desplegarValoresCola(q)

        
if __name__ == "__main__":
    
    q = Queue() # cola donde se almacenan los archivos posteriormente procesados
    
    qRepositorio = Queue() # se guardan aquí los números encontrados hasta que se llene con la cantidad solicitada
    
    e = Event() 
    
    nr_buscadores = 4 # modificable, se puede poner cualquier número de buscadores
    
    nr_datos_requeridos = 10 # modificable, se puede poner cualquier cantidad que se quiera encontrar
    
    for i in range(10): # los datos se han creado previamente con archivo genera-datos.py
        q.put("datos"+str(i))
    
#    q.put("avanz1.txt")
#    q.put("avanz2.txt")
    
    procesos = [Process(target=buscarEnCola, args=(gestor,condicion,q,qRepositorio,e, nr_datos_requeridos)) for i in range(nr_buscadores)]    
    
    e.set() 
    
    for p in procesos:
        p.start()    
        
    for p in procesos:
        p.join()
    
# Si se inhabilita el evento -clear- es porque la función gestor ha 
# inhabilitado el evento, pero esta inhabilitación solo se 
# realizará si y solo si la cantidad de números que cumplen la 
# condición es la solicitada (mayor o igual en verdad). 
# Por lo tanto, el evento seguirá establecido (e.is_set()==True) si y solo si la cantidad
# de numeros que cumplen la condición recolectados -en qRepositorio- 
# es menor que la solicitada. Solo bajo esta premisa es que el 
# siguiente condicional se satisface y tiene sentido ponerlo aquí.          
    if e.is_set(): 
        
        print("Se han obtenido solo "+str(qRepositorio.qsize())+" numeros que cumplen la condición.\n", flush=True) 
        
        desplegarValoresCola(qRepositorio)
  
    input()



