from math import sqrt, floor
from multiprocessing import Queue, Event, Process, current_process
import time

def esPrimo(numero: int) -> bool:
    if numero < 2:
        return False
    for i in range(2, floor(sqrt(numero) + 1)):
        if numero % i == 0:
            return False
    return True

def acabaEnN(N: int, numero: int) -> bool:
    return numero % (10**len(str(N))) == N

def acabaEn227(numero: int) -> bool:
    return acabaEnN(227, numero)

def esPrimoYAcabaEn227(numero: int) -> bool:
    return esPrimo(numero) and acabaEn227(numero)

def meterEnCola(cola, listaArchivos: list) -> None:
    for nombreArchivo in listaArchivos:
        cola.put(nombreArchivo)

def buscarNumerosPrimos(condicion: callable, cola_archivos, cantidad_primos, evento) -> None:
    primos_encontrados = 0
    while primos_encontrados < cantidad_primos and not cola_archivos.empty():
        archivo = cola_archivos.get()
        try:
            with open(archivo) as f:
                contenido = f.read()
                listaNumeros = list(map(int, contenido.split(' ')))
                for num in listaNumeros:
                    if condicion(num):
                        primos_encontrados += 1
                        print(f'Proceso {current_process().name} encontró el número primo: {num}')
                    if primos_encontrados >= cantidad_primos:
                        evento.set()
                        return
        except Exception as e:
            print(f"Error al leer el archivo {archivo}: {str(e)}")

if __name__ == "__main__":
    colaArchivos = Queue()
    listaArchivos = ['datos0', 'datos1', 'datos2', 'datos3', 'datos4', 'datos5', 'datos6', 'datos7', 'datos8', 'datos9']
    meterEnCola(colaArchivos, listaArchivos)
            
    encontradosNPrimos = Event() # Este evento se establecerá cuando se hayan encontrado los N números primos deseados
    cantidad_primos_deseados = 5  # Modifica aquí la cantidad de primos que deseas encontrar

    # Creamos los procesos buscadores
    numeroDeProcesos = 10
    listaProcesos = []
    for i in range(numeroDeProcesos):
        proceso = Process(target=buscarNumerosPrimos, args=(esPrimoYAcabaEn227, colaArchivos, cantidad_primos_deseados, encontradosNPrimos,))
        listaProcesos.append(proceso)
    
    t1 = time.perf_counter()
    
    # Iniciamos los procesos
    for proceso in listaProcesos:
        proceso.start()
    
    # Esperamos hasta que se establezca el evento (se encuentren los números primos deseados) o todos los procesos terminen
    encontradosNPrimos.wait()

    # Terminamos todos los procesos restantes
    for proceso in listaProcesos:
        proceso.terminate()
    
    # Esperamos a que todos los procesos terminen
    for proceso in listaProcesos:
        proceso.join()
    
    t2 = time.perf_counter()
    
    tiempo_ejecucion = t2 - t1
    print(f"Tiempo de ejecución: {tiempo_ejecucion}")
