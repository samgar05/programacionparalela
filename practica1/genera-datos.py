from random import randint
from os import path, makedirs
from math import ceil, floor


def crear_archivos_de_datos(nr_archivos: int, nr_datos_por_archivo: int,
                            dato_min: int, dato_max: int,
                            directorio: str) -> None:
    if not path.exists(directorio):
        makedirs(directorio)
    for i in range(nr_archivos):
        nr_datos = randint(ceil(0.9 * nr_datos_por_archivo),
                           floor(1.1 * nr_datos_por_archivo))
        # o, para que todos los archivos tengan igual cantidad de datos:
        #     nr_datos = nr_datos_por_archivo
        nombre_archivo = f'datos{i}'
        ruta_archivo = path.join(directorio, nombre_archivo)
        with open(ruta_archivo, 'w') as archivo:
            numeros = [str(randint(dato_min, dato_max))
                       for _ in range(nr_datos)]
            archivo.write(' '.join(numeros))


if __name__ == "__main__":
    # Parámetros
    nr_archivos = 10
    nr_datos_por_archivo = 1_000_0
    dato_min = 1
    dato_max = 1_000_000
    #directorio = "F:/DEV/programacionparalela/practica1"
    directorio = "C:\DEV\ProgParalela\programacionparalela\practica1" #en laptop

    # Ejecutar la función para generar los archivos
    crear_archivos_de_datos(nr_archivos, nr_datos_por_archivo, dato_min, dato_max, directorio)
