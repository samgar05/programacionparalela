# buscamos de entre unos cuantos archivos de texto un numero primo acabado en ...227
# los procesos van a archivos distintos 

from math import sqrt, floor
from multiprocessing import Queue, Event, Process
import time



# funcion para verificar si un numero es primo

def esPrimo(numero:int) -> bool:
    if numero<2:
        return False
    for i in range(2, floor(sqrt(numero) +1)):
        if numero % i == 0:
            return False
    return True

def acabaEnN(N:int, numero:int) -> bool:
    return numero % (10**len(str(N))) == N

def acabaEn227(numero:int) -> bool:
    return acabaEnN(227,numero)

def esPrimoYAcabaEn227(numero:int) -> bool:
    return esPrimo(numero) and acabaEn227(numero)


def meterEnCola(cola,listaArchivos:list) -> None:
    for nombreArchivo in listaArchivos:
        cola.put(nombreArchivo)


def buscarNumero(condicion:callable, cola_archivos, evento) -> None:
    while cola_archivos.qsize()>0:
        if evento.is_set():
            break
        else:
            archivo = cola_archivos.get()
            contenido = (open(archivo)).read()
            listaNumeros = list(map(int, contenido.split(' ')))
            for num in listaNumeros:
                if condicion(num):
                    evento.set()
                    print(f'Numero {num}')
                    break
           
        
    
def buscarNNumeros(condicion:callable, n, cola_archivos, cola_encontrados, evento) -> None:
    while cola_archivos.qsize()>0 and not evento.is_set() and cola_encontrados.qsize() < n:
        pass



if __name__ == "__main__":

    colaArchivos = Queue()
    listaArchivos = ['datos0', 'datos1','datos2','datos3','datos4','datos5','datos6', 'datos7', 'datos8', 'datos9']
    meterEnCola(colaArchivos, listaArchivos)
            
    encontradoElNumero = Event() # conviene definir el evento como haber encontrado el numero o estar buscando
    #encontradosN = Event()  # para el caso de encontrar N elementos que cumplen
    
    #  creamos unos cuantos procesos buscadores
    numeroDeProcesos = 10
    listaProcesos = []
    for _ in range(numeroDeProcesos):
        proceso = Process(target=buscarNumero, args = (esPrimoYAcabaEn227, colaArchivos, encontradoElNumero,))
        listaProcesos.append(proceso)
    
    t1 = time.perf_counter()
    
    for proceso in listaProcesos:
        proceso.start()
    
    encontradoElNumero.wait(timeout = 40)
    
    if encontradoElNumero.is_set():
        for proceso in listaProcesos:
            proceso.terminate()
            proceso.join()
            
    
    t2 = time.perf_counter()
    
    tiempo_ejecucion = t2 - t1
    print(f"Tiempo de ejecución: {tiempo_ejecucion}")

        
    
# dar una cantidad de elementos y una condicion y encontrar ese numero de elementos que cumplen esa condicion
# qsize() tamano de una cola


    
