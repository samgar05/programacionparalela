from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
from BaseDeDatos import BaseDeDatos, baseDatos

def manejar_cliente(client_socket):
    orden_recibida = client_socket.recv(1024)
    mensaje_recibido = client_socket.recv(1024)
    
    while orden_recibida:
        orden = orden_recibida.decode()
        
        if orden == '1':
            pregunta = mensaje_recibido.decode()
            respuesta = pregunta.upper()
        elif orden == '2':
            pregunta = mensaje_recibido.decode()
            respuesta = pregunta.lower()
        elif orden == '3':
            pregunta = mensaje_recibido.decode()
            respuesta = pregunta[::-1]
        else:
            respuesta = mensaje_recibido.decode()
        
        mensaje_enviado = respuesta.encode()
        client_socket.send(mensaje_enviado)
        
        orden_recibida = client_socket.recv(1024)
        mensaje_recibido = client_socket.recv(1024)
    
    client_socket.close()

server_socket = socket(AF_INET, SOCK_STREAM) #protocolo
ip_addr = 'localhost'
puerto = 54321
server_socket.bind((ip_addr, puerto))
server_socket.listen()

while True:
    client_socket, _ = server_socket.accept()
    client_thread = Thread(target=manejar_cliente, args=(client_socket,))
    client_thread.start()
