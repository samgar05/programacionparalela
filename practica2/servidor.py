from socket import socket

srvr_socket = socket()
ip_addr = 'localhost' #ip del mismo servidor
#ip_addr = '10.8.108.87'
puerto = 54321
srvr_socket.bind((ip_addr, puerto))
srvr_socket.listen()

cl_socket, _ = srvr_socket.accept()

msj_rec = cl_socket.recv(1024)
pregunta = msj_rec.decode()
respuesta = pregunta.upper()
msj_env = respuesta.encode()
cl_socket.send(msj_env) # se lo reenvia cambiado el cliente

cl_socket.close()

srvr_socket.close()
