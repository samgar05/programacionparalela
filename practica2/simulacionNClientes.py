import socket
import threading
import time

def cliente(numero):
    server_ip = 'localhost'
    server_port = 54321

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((server_ip, server_port))

    ordenes_textos = [
        ('1', 'Saludos a todos'),
        ('2', 'Descubrimiento'),
        ('3', 'Números y cifras'),
        ('1', 'Expresión de aprecio'),
        ('2', 'Unidad y conjunción'),
        ('3', 'Secuencia numérica'),
        ('1', 'Conectividad global'),
        ('2', 'Conocimiento adquirido'),
        ('3', 'Orden alfabético'),
        ('1', 'Interacción virtual'),
        ('2', 'Inteligencia sintética'),
        ('3', 'Secuencia de teclas'),
        ('1', 'Plataforma dinámica'),
        ('2', 'Red neuronal recursiva'),
        ('3', 'Secuencia inversa'),
        ('1', 'Análisis de datos'),
        ('2', 'Red neuronal convolucional'),
        ('3', 'Secuencia de caracteres'),
        ('1', 'Desarrollo de software'),
        ('2', 'Procesamiento de lenguaje natural'),
        ('3', 'Secuencia alfabética inversa'),
        ('1', 'Rama de la informática'),
        ('2', 'Red neuronal adversarial generativa'),
        ('3', 'Secuencia de caracteres invertida'),
        ('1', 'Aprendizaje automatizado'),
        ('2', 'Máquina de vectores de soporte'),
        ('3', 'Secuencia alfabética'),
        ('1', 'Aprendizaje profundo'),
        ('2', 'Bosque aleatorio'),
        ('3', 'Secuencia alfabética inversa')
    ]

    for orden, texto in ordenes_textos:
        print(f"Cliente {numero} enviando orden al servidor:", orden)
        client_socket.sendall(orden.encode())

        time.sleep(0.3)

        print(f"Cliente {numero} enviando mensaje al servidor:", texto)
        client_socket.sendall(texto.encode())

        respuesta = client_socket.recv(1024).decode()
        print(f"-------Respuesta del servidor al cliente {numero}:", respuesta)

    client_socket.close()

if __name__ == "__main__":
    threads = []
    for i in range(5): #numero de clientes simulados
        thread = threading.Thread(target=cliente, args=(i+1,))
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()