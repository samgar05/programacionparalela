import subprocess
import time

# Define las órdenes y los textos para cada cliente
ordenes_textos = [
    ('1', 'Hola Mundo'),
    ('2', 'PYTHON'),
    ('3', '123456'),
    ('1', 'OpenAI'),
    ('2', 'MACHINE LEARNING'),
    ('3', 'abcdefg'),
    ('1', 'Internet de las Cosas'),
    ('2', 'DEEP LEARNING'),
    ('3', '987654321'),
    ('1', 'ChatGPT'),
    ('2', 'ARTIFICIAL INTELLIGENCE'),
    ('3', 'qwerty'),
    ('1', 'Python es genial'),
    ('2', 'RECURSIVE NEURAL NETWORK'),
    ('3', 'zxcvbnm'),
    ('1', 'Ciencia de Datos'),
    ('2', 'CONVOLUTIONAL NEURAL NETWORK'),
    ('3', 'asdfghjkl'),
    ('1', 'Programación'),
    ('2', 'NATURAL LANGUAGE PROCESSING'),
    ('3', 'poiuytrewq'),
    ('1', 'Inteligencia Artificial'),
    ('2', 'GENERATIVE ADVERSARIAL NETWORK'),
    ('3', 'mnbvcxz'),
    ('1', 'Machine Learning'),
    ('2', 'SUPPORT VECTOR MACHINE'),
    ('3', 'abcdefghijklm'),
    ('1', 'Deep Learning'),
    ('2', 'RANDOM FOREST'),
    ('3', 'nopqrstuvwxyz')
]

# Ejecuta el cliente con peticiones distintas para cada cliente
for i in range(30):
    orden, texto = ordenes_textos[i % len(ordenes_textos)]
    comando = f"miclienteparte1y2.py '{orden}' '{texto}'"
    subprocess.Popen(comando, shell=True)
    time.sleep(0.1)  # Espera breve entre el inicio de cada cliente