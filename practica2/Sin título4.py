import socket
import pickle

# Configuración del cliente
HOST = '127.0.0.1'
PORT = 12345

# Función para enviar solicitudes al servidor
def send_request(request):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
        client_socket.connect((HOST, PORT))
        client_socket.send(pickle.dumps(request))
        response = client_socket.recv(1024)
        print("Respuesta del servidor:", pickle.loads(response))

# Ejemplos de solicitudes al servidor
# Modifica según las operaciones que quieras realizar
send_request({"operacion": "alta", "clave": 36, "nombre": "Ana", "edad": 28})
send_request({"operacion": "alta", "clave": 37, "nombre": "Carlos", "edad": 27})
send_request({"operacion": "modificar", "clave_vieja": 36, "clave_nueva": 38})
send_request({"operacion": "baja", "clave": 37})

