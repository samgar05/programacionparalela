#Ver 2pr2-servidor.py del campus virtual para una plantilla de las primeras dos partes

from socket import socket

server_socket = socket()
ip_addr = 'localhost' #ip del mismo servidor
#ip_addr = '10.8.108.87'
puerto = 54321
server_socket.bind((ip_addr, puerto))
server_socket.listen()

def mayus(string:str) -> str:
    return string.upper()

def minus(frase: str) -> str:
    return frase.lower()


def invertir(frase: str) -> str:
    return frase[::-1]

def d():
    pass

client_socket, _ = server_socket.accept()

orden_recibida = client_socket.recv(1024)
mensaje_recibido = client_socket.recv(1024)
while orden_recibida:
    
    orden = orden_recibida.decode()
    #esto se puede arreglar en un futuro con un sistema de listas o algo por el estilo
    if orden == '1':
        pregunta = mensaje_recibido.decode()
        respuesta = pregunta.upper()
    
    elif orden == '2':
        pregunta = mensaje_recibido.decode()
        respuesta = pregunta.lower()
    
    elif orden == '3':
        pregunta = mensaje_recibido.decode()
        respuesta = pregunta[::-1]
        
    else:
        respuesta = pregunta
    
    mensaje_enviado = respuesta.encode()
    client_socket.send(mensaje_enviado)
    
    orden_recibida = client_socket.recv(1024) #escucha unos nuevos orden y mensaje
    mensaje_recibido = client_socket.recv(1024)
    
    

#orden_recibida = client_socket.recv(1024) 
#mensaje_recibido = client_socket.recv(1024)

client_socket.close()
server_socket.close()

#Queremos ampliarlo para poder aceptar varios servicios distintos 
#(al menos) tres servicios distintos: transformar a mayúsculas, transformar a minúsculas e invertir la cadena de caracteres dada.

'''
Para las partes posteriores, cuando hagamos varios clientes nos puede venir bien considerar un código (análogo a un dni por ejemplo) para 
una tupla del estilo (Sam, 2001, Madrid) en un dict de python {32:(Sam, 2001, Madrid)}

Se sugiere que sea una base de datos separada

'''

#Parte 3 Ahora queremos que haya un socket por cada cliente y que cada socket reciba un tipo de datos como pueden ser ordenes o mensajes