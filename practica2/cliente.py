from socket import socket

sckt = socket()
srvr_ip = 'localhost' # ip del cliente al que quiere conectarse
# srvr_ip = '192.168.0.29'
srvr_puerto = 54321

sckt.connect((srvr_ip, srvr_puerto)) 

pregunta = 'Hola'
msj_env = pregunta.encode() # escucha el mensaje que le llega
sckt.send(msj_env) #
msj_rec = sckt.recv(1024)
respuesta = msj_rec.decode()
print(f'según el servidor, "{pregunta}" en mayúsculas es "{respuesta}"')

sckt.close()
