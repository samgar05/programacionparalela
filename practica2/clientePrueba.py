import socket
from time import sleep

def main():
    server_ip = 'localhost'
    server_port = 54321

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((server_ip, server_port))

    ordenes = {'1':'Cambiar texto a mayusculas','2':'Cambiar texto a minusculas','3':'Invertir texto'}

    ordenes_textos = [
        ('1', 'Hola Mundo'),
        ('2', 'PYTHON'),
        ('3', '123456'),
        ('1', 'OpenAI'),
        ('2', 'MACHINE LEARNING'),
        ('3', 'abcdefg'),
        ('1', 'Internet de las Cosas'),
        ('2', 'DEEP LEARNING'),
        ('3', '987654321'),
        ('1', 'ChatGPT'),
        ('2', 'ARTIFICIAL INTELLIGENCE'),
        ('3', 'qwerty'),
        ('1', 'Python es genial'),
        ('2', 'RECURSIVE NEURAL NETWORK'),
        ('3', 'zxcvbnm'),
        ('1', 'Ciencia de Datos'),
        ('2', 'CONVOLUTIONAL NEURAL NETWORK'),
        ('3', 'asdfghjkl'),
        ('1', 'Programación'),
        ('2', 'NATURAL LANGUAGE PROCESSING'),
        ('3', 'poiuytrewq'),
        ('1', 'Inteligencia Artificial'),
        ('2', 'GENERATIVE ADVERSARIAL NETWORK'),
        ('3', 'mnbvcxz'),
        ('1', 'Machine Learning'),
        ('2', 'SUPPORT VECTOR MACHINE'),
        ('3', 'abcdefghijklm'),
        ('1', 'Deep Learning'),
        ('2', 'RANDOM FOREST'),
        ('3', 'nopqrstuvwxyz')
    ]## 30 elementos

    for orden, texto in ordenes_textos:
        print("Enviando orden al servidor:", orden)  # Añadido para depurar
        client_socket.sendall(orden.encode())
        
        sleep(0.3)

        print("Enviando mensaje al servidor:", texto)  # Añadido para depurar
        client_socket.sendall(texto.encode())

        respuesta = client_socket.recv(1024).decode()
        print("-------RESPUESTA DEL SERVIDOR:", respuesta)

    client_socket.close()

if __name__ == "__main__":
    main()
