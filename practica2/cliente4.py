from socket import socket

# Configurar el socket del cliente
sckt = socket()
srvr_ip = "localhost"
srvr_puerto = 54321

# Conectar al servidor
sckt.connect((srvr_ip, srvr_puerto))

# Definir las opciones disponibles
dic = {1: "Dar de alta", 2: "Dar de baja", 3: "Modificar entrada", 4: "Consultar", 5: "Ver toda la base de datos", 6: "Salir"}
lst = list(dic)

# Mostrar las opciones disponibles al usuario
print("Peticiones:\n" + str(lst[0]) + "." + dic[1] + "\n" + str(lst[1]) + "." + dic[2] + "\n" + str(lst[2]) + "." + dic[3] + "\n" + str(lst[3]) + "." + dic[4] + "\n" + str(lst[4]) + "." + dic[5] + "\n" + str(lst[5]) + "." + dic[6])
print()

# Proceso de solicitud
while True:
    tipoPet = input("Introducir número de petición: ")
    tipoPet = int(tipoPet)

    if tipoPet == 6:
        # Salir del bucle si la opción es "Salir"
        break

    else:
        # Enviar la opción seleccionada al servidor
        sckt.send(str(tipoPet).encode())

        # Manejar la respuesta del servidor
        respuesta = sckt.recv(1024).decode()
        print(respuesta)

        # Si la opción requiere datos adicionales, solicitarlos al usuario y enviarlos al servidor
        if tipoPet in [1, 2, 3]:
            datos = input("Ingrese los datos necesarios (ID, Nombre, Edad): ")
            sckt.send(datos.encode())

# Cerrar la conexión con el servidor
sckt.close()
