#Ver 2pr2-cliente.py del campus virtual para una plantilla de las primeras dos partes

from socket import socket

sckt = socket()
srvr_ip = 'localhost'
# srvr_ip = '192.168.0.29'
srvr_puerto = 54321

sckt.connect((srvr_ip, srvr_puerto))

numero_de_peticiones = 3

ordenes = {'1':'Cambiar texto a mayusculas','2':'Cambiar texto a minusculas','3':'Invertir texto'}

for _ in range(numero_de_peticiones):
    #le pedimos al cliente que escriba un texto, lo manda por el socket hasta el servidor, lo procesa y se lo manda.
    print("Introduzca una orden:\n1-Cambiar texto a mayusculas\n2-Cambiar texto a minusculas\n3-Invertir texto")
    orden = input()
    print("Introduzca el texto a transformar:")
    texto = input()
    
    orden_enviada = orden.encode()
    sckt.send(orden_enviada)
    
    texto_enviado = texto.encode()
    sckt.send(texto_enviado)
    
    
    msj_rec = sckt.recv(1024)
    respuesta = msj_rec.decode()
    
    print(f'Se ha recibido la orden {ordenes[orden]}')
    print(f'según el servidor, "{texto}" en mayúsculas es\n"{respuesta}"\n')
    


sckt.close()
