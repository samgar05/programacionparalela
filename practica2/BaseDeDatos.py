#Base de Datos para el alta baja y modificacion y consulta

class BaseDeDatos:
    def __init__(self):
        self.data = {}
        self.ids = []
    
    def alta(self, clave, nombre, edad):
        if clave in self.ids:
            print("El ID ya está registrado")
        else:
            self.data[clave] = {'Nombre': nombre, 'Edad': edad}
            self.ids.append(clave)
    
    def baja(self, clave):
        if clave in self.ids:
            del self.data[clave]
            self.ids.remove(clave)
            print(f"Se ha borrado la entrada número {clave}")
        else:
            print("No se ha encontrado el ID")
            
    def modificar(self, clave_vieja, clave_nueva, default=None):
        if clave_vieja in self.ids:
            self.data[clave_nueva] = self.data.pop(clave_vieja, default)
            self.ids[self.ids.index(clave_vieja)] = clave_nueva
        else:
            print("No se ha encontrado el ID")
    
    def consulta(self, clave):
        if clave in self.data:
            return self.data[clave]
        else:
            print("No se ha encontrado el ID")
            return None
    
#### Ejemplo de diccionario de la forma particular
baseDatos = BaseDeDatos()

datos_personales = {
    1: {"nombre": "Juan", "edad": 25},
    2: {"nombre": "María", "edad": 30},
    3: {"nombre": "Pedro", "edad": 35},
    4: {"nombre": "Ana", "edad": 28},
    5: {"nombre": "Luis", "edad": 40},
    6: {"nombre": "Sofía", "edad": 22},
    7: {"nombre": "Carlos", "edad": 27},
    8: {"nombre": "Laura", "edad": 33},
    9: {"nombre": "Diego", "edad": 29},
    10: {"nombre": "Lucía", "edad": 32},
    11: {"nombre": "Elena", "edad": 31},
    12: {"nombre": "Javier", "edad": 26},
    13: {"nombre": "Patricia", "edad": 38},
    14: {"nombre": "Andrés", "edad": 34},
    15: {"nombre": "Marta", "edad": 37},
    16: {"nombre": "David", "edad": 23},
    17: {"nombre": "Carmen", "edad": 36},
    18: {"nombre": "Roberto", "edad": 39},
    19: {"nombre": "Isabel", "edad": 24},
    20: {"nombre": "Alejandro", "edad": 41},
    21: {"nombre": "Eva", "edad": 28},
    22: {"nombre": "Raul", "edad": 33},
    23: {"nombre": "Paula", "edad": 29},
    24: {"nombre": "Sergio", "edad": 35},
    25: {"nombre": "Natalia", "edad": 31},
    26: {"nombre": "Mario", "edad": 27},
    27: {"nombre": "Silvia", "edad": 30},
    28: {"nombre": "Hector", "edad": 36},
    29: {"nombre": "Cristina", "edad": 34},
    30: {"nombre": "Daniel", "edad": 39}
}

for key in datos_personales.keys():
    baseDatos.alta(key,datos_personales[key]["nombre"], datos_personales[key]["edad"])
