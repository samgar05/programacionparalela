import socket
import threading
from BaseDeDatos import BaseDeDatos

# Función que maneja las conexiones de los clientes
def handle_client(client_socket, bd):
    while True:
        try:
            # Recibir la solicitud del cliente
            data = client_socket.recv(1024)
            if not data:
                break
            
            # Decodificar la solicitud
            request = data.decode("utf-8").split('|')
            operacion = request[0]
            parametros = request[1:]
            
            # Ejecutar la solicitud y obtener la respuesta
            if operacion == "alta":
                response = bd.alta(*parametros)
            elif operacion == "baja":
                response = bd.baja(*parametros)
            elif operacion == "modificar":
                response = bd.modificar(*parametros)
            elif operacion == "consulta":
                response = bd.consulta(*parametros)
            else:
                response = "Operación no válida"
            
            # Enviar la respuesta al cliente junto con el estado actualizado de la base de datos
            response += '|' + str(bd.data)
            client_socket.send(response.encode("utf-8"))
            
            # Imprimir la solicitud recibida y la respuesta enviada
            print("Solicitud recibida:", operacion, parametros)
            print("Respuesta enviada:", response)
                
        except Exception as e:
            print("Error:", e)
            break
    
    # Cerrar la conexión con el cliente
    client_socket.close()

# Configuración del servidor
HOST = '127.0.0.1'
PORT = 12345

# Inicializar la base de datos
baseDatos = BaseDeDatos()

datos_personales = {
    1: {"nombre": "Juan", "edad": 25},
    2: {"nombre": "María", "edad": 30},
    3: {"nombre": "Pedro", "edad": 35},
    4: {"nombre": "Ana", "edad": 28},
    5: {"nombre": "Luis", "edad": 40},
    6: {"nombre": "Sofía", "edad": 22},
    7: {"nombre": "Carlos", "edad": 27},
    8: {"nombre": "Laura", "edad": 33},
    9: {"nombre": "Diego", "edad": 29},
    10: {"nombre": "Lucía", "edad": 32},
    11: {"nombre": "Elena", "edad": 31},
    12: {"nombre": "Javier", "edad": 26},
    13: {"nombre": "Patricia", "edad": 38},
    14: {"nombre": "Andrés", "edad": 34},
    15: {"nombre": "Marta", "edad": 37},
    16: {"nombre": "David", "edad": 23},
    17: {"nombre": "Carmen", "edad": 36},
    18: {"nombre": "Roberto", "edad": 39},
    19: {"nombre": "Isabel", "edad": 24},
    20: {"nombre": "Alejandro", "edad": 41},
    21: {"nombre": "Eva", "edad": 28},
    22: {"nombre": "Raul", "edad": 33},
    23: {"nombre": "Paula", "edad": 29},
    24: {"nombre": "Sergio", "edad": 35},
    25: {"nombre": "Natalia", "edad": 31},
    26: {"nombre": "Mario", "edad": 27},
    27: {"nombre": "Silvia", "edad": 30},
    28: {"nombre": "Hector", "edad": 36},
    29: {"nombre": "Cristina", "edad": 34},
    30: {"nombre": "Daniel", "edad": 39}
}

for key in datos_personales.keys():
    baseDatos.alta(key,datos_personales[key]["nombre"], datos_personales[key]["edad"])


# Iniciar el servidor
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen(5)
print(f"Servidor escuchando en {HOST}:{PORT}")

# Manejar las conexiones de los clientes
while True:
    client_socket, addr = server_socket.accept()
    print(f"Cliente conectado desde {addr}")
    
    # Crear un hilo para manejar las solicitudes del cliente
    client_thread = threading.Thread(target=handle_client, args=(client_socket, baseDatos))
    client_thread.start()
