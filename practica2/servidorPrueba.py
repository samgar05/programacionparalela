from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread



def manejar_cliente(client_socket):
    while True:
        orden_recibida = client_socket.recv(1024).decode()
        if not orden_recibida:
            break
        
        if orden_recibida == '1':
            mensaje_recibido = client_socket.recv(1024).decode()
            respuesta = mensaje_recibido.upper()
        elif orden_recibida == '2':
            mensaje_recibido = client_socket.recv(1024).decode()
            respuesta = mensaje_recibido.lower()
        elif orden_recibida == '3':
            mensaje_recibido = client_socket.recv(1024).decode()
            respuesta = mensaje_recibido[::-1]
        else:
            respuesta = "Orden no reconocida"

        print("Enviando respuesta al cliente:", respuesta)  # Añadido para depurar
        client_socket.send(respuesta.encode())

        import time
        time.sleep(0.5)

server_socket = socket(AF_INET, SOCK_STREAM)
ip_addr = 'localhost'
puerto = 54321
server_socket.bind((ip_addr, puerto))
server_socket.listen()

print("Servidor iniciado. Esperando conexiones...")

while True:
    client_socket, _ = server_socket.accept()
    print("Conexión aceptada desde:", _)
    client_thread = Thread(target=manejar_cliente, args=(client_socket,))
    client_thread.start()
