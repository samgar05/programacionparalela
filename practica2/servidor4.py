from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
from BaseDeDatos import baseDatos

def manejar_cliente(client_socket):
    while True:
        # Recibir la orden del cliente
        orden_recibida = client_socket.recv(1024).decode()
        if not orden_recibida:
            break
        orden = int(orden_recibida)
        
        # Procesar la orden
        if orden == 1:
            datos_recibidos = client_socket.recv(1024).decode()
            idPeticion, nombrePeticion, edadPeticion = datos_recibidos.split()
            baseDatos.alta(idPeticion, nombrePeticion, edadPeticion)
            respuesta = "Se ha dado de alta la entrada"
            
        elif orden == 2:
            idPeticion = int(client_socket.recv(1024).decode())
            baseDatos.baja(idPeticion)
            respuesta = "Se ha dado de baja la entrada"
            
        elif orden == 3:
            datos_recibidos = client_socket.recv(1024).decode()
            idPeticion, idNueva = datos_recibidos.split()
            baseDatos.modificar(idPeticion, int(idNueva))
            respuesta = "Se ha modificado la entrada"
            
        elif orden == 4:
            idPeticion = int(client_socket.recv(1024).decode())
            respuesta = str(baseDatos.consulta(idPeticion))
            
        elif orden == 5:
            respuesta = str(baseDatos.data)
            
        else:
            respuesta = "Petición no válida"
        
        # Enviar respuesta al cliente
        respuesta_serializada = respuesta.encode()
        client_socket.send(respuesta_serializada)
    
    # Cerrar la conexión con el cliente
    client_socket.close()

# Configurar el socket del servidor
server_socket = socket(AF_INET, SOCK_STREAM)
ip_addr = 'localhost'
puerto = 54321
server_socket.bind((ip_addr, puerto))
server_socket.listen()

# Esperar conexiones entrantes
while True:
    client_socket, _ = server_socket.accept()
    client_thread = Thread(target=manejar_cliente, args=(client_socket,))
    client_thread.start()
