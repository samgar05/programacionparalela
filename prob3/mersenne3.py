"""
En esta versión, hay un proceso más que se encarga de poner los números
primos en la cola de tareas en paralelo al funcionamiento de los trabajadores
y del proceso principal.
"""


from multiprocessing import Process, Queue


def lucas_lehmer(p: int) -> bool:
    if p == 2:
        return True
    m = 2**p - 1
    s = 4
    for _ in range(p - 2):
        s = (s * s - 2) % m
    return s == 0


def es_primo(n: int) -> bool:
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def encola_primos_y_nones(q_tareas: Queue, nr_trab: int) -> None:
    for p in range(2, 10000):
        if es_primo(p):
            q_tareas.put(p)
    for _ in range(nr_trab):
        q_tareas.put(None)


def comprueba(q_tareas: Queue, q_res: Queue) -> None:
    p = q_tareas.get()
    while p is not None:
        if lucas_lehmer(p):
            q_res.put((p, True))
        else:
            q_res.put((p, False))
        p = q_tareas.get()
    q_res.put(None)


def main() -> None:
    nr_trabajadores = 4
    q_tareas = Queue()
    q_res = Queue()

    generadora = Process(target=encola_primos_y_nones, args=(q_tareas, nr_trabajadores))
    generadora.start()

    trabajadores = [Process(target=comprueba, args=(q_tareas, q_res))
                    for _ in range(nr_trabajadores)]
    for trab in trabajadores:
        trab.start()

    nr_nones = 0
    while nr_nones < nr_trabajadores:
        res = q_res.get()
        if res is None:
            nr_nones += 1
        else:
            p, is_prime = res
            if is_prime:
                print(f"M_{p} es primo")
            # else:
            #     print(f"M_{p} no es primo")
    print("hemos acabado")

    generadora.join()
    for trab in trabajadores:
        trab.join()


if __name__ == '__main__':
    main()
