"""
Esta es la versión básica, en la que el proceso principal pone todos los
exponentes en una cola al principio y los trabajadores imprimen ellos
mismos los resultados.
"""


from multiprocessing import Process, Queue


def lucas_lehmer(p: int) -> bool:
    if p == 2:
        return True
    m = 2**p - 1
    s = 4
    for _ in range(p - 2):
        s = (s * s - 2) % m
    return s == 0


def es_primo(n: int) -> bool:
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def encola_primos(q_tareas: Queue) -> None:
    for p in range(2, 500):
        if es_primo(p):
            q_tareas.put(p)


def comprueba(q_tareas: Queue) -> None:
    while not q_tareas.empty():
        p = q_tareas.get()
        if lucas_lehmer(p):
            print(f"el número M_{p} es primo")
        # else:
        #     print(f"el número M_{p} no es primo")


def main() -> None:
    nr_trabajadores = 4
    q_tareas = Queue()
    encola_primos(q_tareas)

    trabajadores = [Process(target=comprueba, args=(q_tareas,))
                    for _ in range(nr_trabajadores)]
    for trab in trabajadores:
        trab.start()
    for trab in trabajadores:
        trab.join()
    print("hemos acabado")


if __name__ == '__main__':
    main()
