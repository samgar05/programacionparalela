#Ejercicio3 GIMP para pobres 
#exploramos calculo de primos de mersenne con multiprocessing

from multiprocessing import Process, Queue
from math import sqrt, ceil

'''
lista de primos que dan un primo de Mersenne
M = [2, 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 
     521, 607, 1.279, 2.203, 2.281, 3.217, 4.253,
     4.423, 9.689, 9.941 ]
'''

def esPrimo (n:int) -> bool:
    if n == 1:
        return False
    root = ceil(sqrt(n))
    for i in range(2,root+1):
        if n % i == 0:
            return False
    return True

def lucas_lehmer(p: int) -> bool:
    if p == 2:
        return True
    m = 2**p - 1
    s = 4
    for _ in range(p - 2):
        s = (s * s - 2) % m
    return s == 0

def lucasQ(q:Queue) -> None:
    while not q.empty():
        x = q.get()
        if lucas_lehmer(x):
            print('El número {} es exponente de Mersenne.'.format(x))
            
def generar(q:Queue) -> None:
    primos = list(filter(esPrimo, list(range(1,100))))
    for primo in primos:
        q.put(primo)
    

if __name__ == '__main__':

    primos = list(filter(esPrimo, list(range(1,5000))))

    q = Queue()
    for number in primos:
        q.put(number)
        
    p_creacion = Process(target=generar, args=(Queue(),)) 
    #metemos numeros en la cola mientras se producen los resultados
    p_creacion.start()

    procesosDePrimalidad = [Process(target=lucasQ, args=(q,)) for _ in range(10)]
    for p in procesosDePrimalidad:
        p.start()
    for p in procesosDePrimalidad:
        p.join()
        
    p_creacion.join()
    
#Anotaciones sobre la solucion



'''
una funcion para meter en la cola
crea una cola de tareas y un numero de trabajadores


trabajadores = [Process(target=comprueba, args=(q,tareas,))]'''

