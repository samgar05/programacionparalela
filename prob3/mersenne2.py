"""
En esta versión, el proceso principal empieza a recibir e imprimir resultados
sin esperar a que todos los trabajadores terminen.
"""


from multiprocessing import Process, Queue


def lucas_lehmer(p: int) -> bool:
    if p == 2:
        return True
    m = 2**p - 1
    s = 4
    for _ in range(p - 2):
        s = (s * s - 2) % m
    return s == 0


def es_primo(n: int) -> bool:
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def encola_primos(q_tareas: Queue) -> None:
    for p in range(2, 500):
        if es_primo(p):
            q_tareas.put(p)


def comprueba(q_tareas: Queue, q_res: Queue) -> None:
    while not q_tareas.empty():
        p = q_tareas.get()
        if lucas_lehmer(p):
            q_res.put((p, True))
        else:
            q_res.put((p, False))
    q_res.put(None)


def main() -> None:
    nr_trabajadores = 4
    q_tareas = Queue()
    encola_primos(q_tareas)
    q_res = Queue()

    trabajadores = [Process(target=comprueba, args=(q_tareas, q_res))
                    for _ in range(nr_trabajadores)]
    for trab in trabajadores:
        trab.start()

    nr_nones = 0
    while nr_nones < nr_trabajadores:
        res = q_res.get()
        if res is None:
            nr_nones += 1
        else:
            p, is_prime = res
            if is_prime:
                print(f"M_{p} es primo")
            # else:
            #     print(f"M_{p} no es primo")
    print("hemos acabado")

    for trab in trabajadores:
        trab.join()


if __name__ == '__main__':
    main()
