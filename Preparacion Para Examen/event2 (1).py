from multiprocessing import Process, Event
from time import sleep



def tarea1(event: Event) -> None:
    event.set()
    print("ya!")


def tarea2(event: Event) -> None:
    event.wait()
    print("oido")
    event.wait()
    print("oido otra vez")
    event.clear()
    print("olvidado")
    event.wait()
    print("oido por tercera vez")


if __name__ == '__main__':
    global e
    e= Event()

    p1 = Process(target=tarea1, args=(e,))
    p2 = Process(target=tarea2, args=(e,))
    
    
    p1.start()
    sleep(2)
    p2.start()
    p1.join()
    p2.join()
    print("fin")
