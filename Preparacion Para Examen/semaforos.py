from multiprocessing import Lock


class Semaphore1:
    def __init__(self, v_inicial):
        self.__sem = Lock()
        self.__contador = v_inicial
        self.__mutex = Lock()
    
    def wait(self):
        self.__sem.acquire()
        self.__mutex.acquire()
        self.__contador =  self.__contador - 1
        if self.__contador > 0:
            self.__sem.release()
        self.__mutex.release()
        
    def signal(self):
        self.__mutex.acquire()
        
        self.__contador = self.__contador + 1
        if self.__contador == 1:
            self.__sem.release()
        self.__mutex.release()
        
class Semaphore2:
    def __init__(self, v_inicial):
        self.sem = Lock()
        self.contador = v_inicial
        self.mutex = Lock()
    
    def wait(self):
        self.mutex.acquire()
        self.contador += -1
        if self.contador == 0:
            self.sem.acquire()
        self.mutex.release()
        
    def signal(self, v_inicial):
        self.mutex.acquire()
        self.contador += 1
        if self.contador == v_inicial:
            self.sem.acquire()
        self.mutex.release()
        self.sem.release()