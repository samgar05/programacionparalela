from multiprocessing import Pool


lista = [1,2,3,4,5]

def sumar(lista) -> int:
    suma = 0
    for i in lista:
        suma += i
    return suma


if __name__ == '__main__':
    num_procesos = 5
    
    pool = Pool(num_procesos)
    resultados = pool.map(sumar, lista)
    
    print(sum(resultados))