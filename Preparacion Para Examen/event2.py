from multiprocessing import Process, Event


def tarea1(event: Event) -> None:
    event.set()
    print("¡ya!")


def tarea2(event: Event) -> None:
    event.wait()
    print("oído")
    event.wait()
    print("oído otra vez")
    event.clear()
    print("olvidado")
    event.wait()
    print("oído por tercera vez")


if __name__ == '__main__':
    e = Event()
    p1 = Process(target=tarea1, args=(e,))
    p2 = Process(target=tarea2, args=(e,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
    print("fin")
