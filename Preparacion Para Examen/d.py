from multiprocessing import Process, Event
import time


def f(n, event):
    if n == 1:
        print("starting")
        event.wait()
        print("Done!")
    elif n == 2:
        #time.sleep(3)
        event.set()
        print("setting")

if __name__ == "__main__":
    event = Event()  # one instance of Event only

    p1 = Process(target = f, args = (1, event))
    p2 = Process(target = f, args = (2, event))
    p1.start()
    p2.start()
    p1.join()
    p2.join()