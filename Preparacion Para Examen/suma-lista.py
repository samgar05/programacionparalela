import time
import multiprocessing as mp


def sumar(dummy: int) -> int:
    lista = [0,1,2,3,4] * 8_000_000
    return sum(lista)

if __name__ == "__main__":
    t1 = time.perf_counter()
    lista = [0,1,2,3,4] * 32_000_000
    suma = sum(lista)
    t2 = time.perf_counter()
    print(f"suma: {suma}")
    print(f"tiempo con un proceso: {t2-t1} segundos")
    print()

    t1 = time.perf_counter()
    pool = mp.Pool(2)
    resultados = pool.map(sumar, [lista,lista]) # si una lista tiene 10 datos y otro 10 millones cuando acaba con uno usa ese poder con el otro
    suma = sum(resultados)
    t2 = time.perf_counter()
    print(f"suma: {suma}")
    print(f"tiempo con dos procesos en paralelo: {t2-t1} segundos")
    print()