#comprobar si una matriz es simetrica utilizando paralelizacion

import time
import multiprocessing as mp

#naive approach
def simetrica(matriz:list) -> bool:
    numFilas = len(matriz)
    numColumnas = len(matriz[0])
    for i in range(0,numFilas):
        for j in range(0,numColumnas):
            if (matriz[i][j] != matriz[j][i]) and (j != i):
                return False
    return True

def semiSimSup (matriz:list) -> bool: #una matriz se dice semisimetrica si el triangulo superior es simetrico incluyendo la diagonal no principal
    n = len(matriz)
    for i in range(0,n):
        for j in range(0,n):
            if i + j < n and j!= i and matriz[i][j] != matriz[j][i]:
                return False
    return True

def semiSimInf(matriz:list) -> bool:
    n = len(matriz)
    for i in range(0,n):
        for j in range(0,n):
            if i + j >= n and j!= i and matriz[i][j] != matriz[j][i]:
                return False
    return True

def dividirMatriz(matriz:list) -> list:
    return []

'''
def gen(n:int) -> list:
    matriz = np.zeros((n,n), dtype=int)
    for i in range(0,n):
        for j in range(0,n):
            if i + j < n:
                matriz[i][j] = 1
    return matriz
'''

aTsup = [[1,4,3],
         [2,1,0],
         [3,0,0]]
aTinf = [[0,0,0],
         [0,0,2],
         [0,2,1]]

a = [[1,4,3],
     [2,1,2],
     [3,2,1]]

if __name__ == "__main__":
    t1 = time.perf_counter()
    sim = simetrica(a)
    t2 = time.perf_counter()
    print(f"simetrica: {sim}")
    print(f"tiempo con un proceso: {t2-t1} segundos")
    print()
    
    t1 = time.perf_counter()
    pool = mp.Pool(2)
    
    resultados = pool.map(simetrica, [aTsup,aTinf])
    sim = all(list(resultados))
    t2 = time.perf_counter()
    print(f"simetrica: {sim}")
    print(f"tiempo con dos procesos en paralelo: {t2-t1} segundos")
    print()

                