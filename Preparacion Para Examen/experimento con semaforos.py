import multiprocessing as mp


def heLlegado(n:int) -> None:
    print('Ha llegado ' + str(n) + '!' )

if __name__ == '__main__':
    n_procesos = 40
    
    listaProcesos = []
    
    for i in range(0,n_procesos):
        proceso = mp.Process(target = heLlegado(i))
        listaProcesos.append(proceso)
    
    semaforo = mp.Semaphore(2)
    
    for proceso in listaProcesos:
        proceso.start()
    
    for proceso in listaProcesos:
        proceso.join()
        