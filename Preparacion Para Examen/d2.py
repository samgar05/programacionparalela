from multiprocessing import Process
from threading import Thread
from semaforos import Semaphore1, Semaphore2
import time


def my_function(x):
    print(f'Processing {x}')
    
def adios():
    print('todo listo')


def trab(sem, tr_id) -> None:
    sem.wait()
    print(f"{tr_id} trabajando", flush=True)
    time.sleep(1)
    print(f"{tr_id} acabado", flush=True)
    sem.signal(2)
    
if __name__ == '__main__':
    semaforo = Semaphore2(2)
    
    procesos = []
    for i in range(5):
        p = Thread(target = trab, args = (semaforo, i))
        procesos.append(p)
        p.start()
    
    for p in procesos:
        p.join()
    time.sleep(3)
    print('Todo listo')
