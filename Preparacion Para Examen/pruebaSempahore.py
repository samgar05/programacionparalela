from multiprocessing import Semaphore, Process
from threading import Thread
from time import sleep

def f(sem, cont):
    sem.acquire()
    cont -= 1
    print(cont)
    print('trabajando', flush=True)
    sleep(2)
    sem.release()
    cont += 1
    print(cont)
    print('signal', flush=True)


if __name__ == '__main__':
    num_procesos = 18
    cont = 3
    
    sem = Semaphore(3)
    
    procesos = [Thread(target = f, args=(sem,cont)) for _ in range(0,num_procesos)]
    
    for p in procesos:
        p.start()
    
    for p in procesos:
        p.join()
        
    print('terminado', flush=True)