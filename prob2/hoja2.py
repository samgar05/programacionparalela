# calculando medias y desviaciones tipicas de ficheros con muchos numeros
'''
media = suma Xi /n
sigma = sqrt (suma [(Xi - media)^2]/n)
'''
from multiprocessing import Pool
from time import perf_counter


file1 = open("numeros.txt", "r") 
content = file1.read()
listaNumeros = list(map(int, content.split()))
file1.close() 

listaPrueba = []

def media (numLista:list) -> float:
    return sum(numLista)/len(numLista)
    
def sigma (numLista:list) -> float:
    listaCuadrados = list(map(lambda x: (x-media(numLista))**2, numLista))
    return (listaCuadrados/len(numLista))**(1/2)

if __name__ == "__main__":
    t1 = perf_counter()
    media = media(listaPrueba)
    t2 = perf_counter()
    print(f"media: {media}")
    print(f"tiempo con un proceso: {t2-t1} segundos")
    print()

    t1 = perf_counter()
    pool = Pool(2)
    resultados = pool.map(media, [lista,lista]) # si una lista tiene 10 datos y otro 10 millones cuando acaba con uno usa ese poder con el otro
    media = media(resultados)
    t2 = perf_counter()
    print(f"media: {media}")
    print(f"tiempo con dos procesos en paralelo: {t2-t1} segundos")
    print()