from math import sqrt
from time import perf_counter
from multiprocessing import Pool


def suma_sumacuadr_y_cantidad_de_datos(nombre_de_archivo: str) -> tuple[int, int, int]:
    archivo = open(nombre_de_archivo, "r")
    texto = archivo.read()  # por ejemplo: texto = "1 2 3 4 5"
    archivo.close()
    datos_str = texto.split()  # datos_str = ["1", "2", "3", "4", "5"]
    datos = list(map(int, datos_str))  # datos = [1, 2, 3, 4, 5]
    nr_datos = len(datos)   # nr_datos = 5
    suma_datos = sum(datos)  # suma_datos = 15
    suma_cuadrados_datos = sum(dato ** 2 for dato in datos)  # suma_cuadrados_datos = 55
    return suma_datos, suma_cuadrados_datos, nr_datos  # (15, 55, 5)


if __name__ == "__main__":
    nombres_de_archivos = [f"../datos/datos{i}" for i in range(20)]
    # nombres_de_archivos = ["../datos/datos0", ..., "../datos/datos19"]

    nr_procesos = 2

    t1 = perf_counter()

    pool = Pool(nr_procesos)
    resultados = pool.map(suma_sumacuadr_y_cantidad_de_datos, nombres_de_archivos)
    # resultados = [(15, 55, 5), ...]
    suma_total = sum(s for s, _, _ in resultados)
    suma_cuadrados_total = sum(sc for _, sc, _ in resultados)
    nr_datos_total = sum(n for _, _, n in resultados)
    media_total = suma_total / nr_datos_total
    varianza_total = (suma_cuadrados_total / nr_datos_total) - (suma_total / nr_datos_total) ** 2
    desviacion_tipica = sqrt(varianza_total)

    t2 = perf_counter()
    print(f"Media: {media_total}")
    print(f"Desviación típica: {desviacion_tipica}")
    print(f"Tiempo con {nr_procesos} procesos en paralelo: {t2-t1} segundos")
    print()
