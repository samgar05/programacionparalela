#runfile('C:/DEV/ProgParalela/programacionparalela/hoja1.py', wdir='C:/DEV/ProgParalela/programacionparalela')

"""
Metodo de Montecarlo
calculamos los puntos dentro del circulo y estimamos pi con la formula 
pi = 4*areaCirc/areacuad 

Consideramos un circulo de radio 1 y su cuadrado sobreescrito
elejidos entre -1 y
"""
from math import sin, radians
from random import uniform
import time
import multiprocessing as mp

# uniform(-1,1) nos da un numero entre -1 y 1



def generarPuntos(n:int):
    listaDePuntos = []
    for i in range(0,n):
        x = uniform(-1,1)
        y = uniform(-1,1)
        listaDePuntos.append((x,y))
    return listaDePuntos

def comprobarSiEnCirculo (x:float,y:float) -> bool:
    return (x**2 + y**2 <= 1)

def puntosEnElCirculo (listaPuntos:list) -> list:
    listaCirculo = []
    for pair in listaPuntos:
        if comprobarSiEnCirculo(*pair):
            listaCirculo.append(pair)
    return listaCirculo

def calcularPi (numeroIteraciones:int) -> float:
    t1 = time.perf_counter()
    
    listaPuntos = generarPuntos(numeroIteraciones)
    numerosCirculo = len(puntosEnElCirculo(listaPuntos))
    numerosCuadrado = len(listaPuntos)
    estimacion_pi = 4*numerosCirculo/numerosCuadrado
    
    t2 = time.perf_counter()
    print(f"tiempo transcurrido: {t2-t1} segundos")
    return estimacion_pi

#Implementacion con dos procesos paralelos

if __name__ == "__main__":
    print(paralellPi(100,2))

def paralellPi (numPuntos:int, numProcesos:int) -> float:
    t1 = time.perf_counter()
    listaPuntos = [numPuntos//2, numPuntos] # comprobamos cuanto tarda con la mitad y con todos los puntos
    pool = mp.Pool(numProcesos)
    result = pool.map(calcularPi,listaPuntos)
    
    t2 = time.perf_counter()
    print(f"tiempo transcurrido: {t2-t1} segundos")
    
    

#Aguja del bufon
def verSiCortaLaAguja() -> bool:
    x = uniform(0, 1/2)
    theta = uniform(0, 90)
    corta = x <= (1/2) * sin(radians(theta))
    return corta

def generarAgujas(n:int) -> list:
    listaDeAgujas = []
    for i in range(0,n):
        theta = uniform(0,90)
        x = uniform(0,1/2)
        corta = x <= (1/2) * sin(radians(theta))
        (x,theta).append(listaDeAgujas)

#def agujasQueCortan()
'''
def exp_uno():
    
def exp_repe(nr_veces:int) ->
    for _ in range(nr_veces):
        exp_nno()

pool.map(exp_repe, [7,7,7])

'''