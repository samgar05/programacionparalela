from random import uniform
from math import sin, radians
from time import perf_counter
from multiprocessing import Pool


def lanza_una_aguja_y_dime_si_corta() -> bool:
    """ Uso grados en vez de radianes para evitar usar explícitamente π/2,
        que quedaría raro en un programa para estimar π. """
    x = uniform(0, 1/2)  # distancia del centro de la aguja a la línea paralela más cercana
    theta = uniform(0, 90)  # ángulo de la aguja respecto a la línea paralela más cercana (grados)
    corta = x <= (1/2) * sin(radians(theta))
    return corta


def cuantas_cortan(nr_agujas: int) -> int:
    cortan = 0
    for _ in range(nr_agujas):
        if lanza_una_aguja_y_dime_si_corta():
            cortan += 1
    return cortan


def estimar_pi(nr_agujas: int, nr_procesos: int) -> float:
    agujas_por_proceso = nr_agujas // nr_procesos
    nr_agujas = agujas_por_proceso * nr_procesos
    pool = Pool(nr_procesos)
    resultados = pool.map(cuantas_cortan, [agujas_por_proceso] * nr_procesos)
    total_cortan = sum(resultados)
    pi_estimado = 2 / (total_cortan / nr_agujas)
    return pi_estimado


if __name__ == "__main__":
    nr_agujas = 2_000_000
    nr_procesos = 2
    t1 = perf_counter()
    pi_estimado = estimar_pi(nr_agujas, nr_procesos)
    t2 = perf_counter()
    print(f"valor de π estimado: {pi_estimado}")
    print(f"tiempo con {nr_procesos} procesos: {t2 - t1} segundos")
    print()


# if __name__ == "__main__":
#     nr_agujas = 2_000_000
#     for nr_procesos in range(1, 9):
#         t1 = perf_counter()
#         pi_estimado = estimar_pi(nr_agujas, nr_procesos)
#         t2 = perf_counter()
#         print(f"valor de π estimado: {pi_estimado}")
#         print(f"tiempo con {nr_procesos} procesos: {t2 - t1} segundos")
#         print()
