from random import uniform
from time import perf_counter
from multiprocessing import Pool


def esta_en_el_circulo(x: float, y: float) -> bool:
    return x**2 + y**2 <= 1


def cuantos_dentro(nr_puntos: int) -> int:
    dentro = 0
    for _ in range(nr_puntos):
        x, y = uniform(-1, 1), uniform(-1, 1)
        if esta_en_el_circulo(x, y):
            dentro += 1
    return dentro


def estimar_pi(nr_puntos: int, nr_procesos: int) -> float:
    puntos_por_proceso = nr_puntos // nr_procesos  # 100 // 3 = 33
    nr_puntos = puntos_por_proceso * nr_procesos  # 33 * 3 = 99
    pool = Pool(nr_procesos)
    resultados = pool.map(cuantos_dentro, [puntos_por_proceso] * nr_procesos)
    total_dentro = sum(resultados)
    pi_estimado = 4 * total_dentro / nr_puntos
    return pi_estimado


nr_puntos = 2_000_000
for nr_procesos in range(1, 8):
    t1 = perf_counter()
    pi_estimado = estimar_pi(nr_puntos, nr_procesos)
    t2 = perf_counter()
    print(f"valor de π estimado: {pi_estimado}")
    print(f"tiempo con {nr_procesos} procesos: {t2 - t1} segundos")
    print()



#t1 = perf_counter()
#pi_estimado = 4 * cuantos_dentro(2_000_000) / 2_000_000
#t2 = perf_counter()
#print(f"valor de π estimado: {pi_estimado}")
#print(f"tiempo con un proceso: {t2-t1} segundos")
#print()


#t1 = perf_counter()
#resultados = map(cuantos_dentro, [1_000_000, 1_000_000])
#total_dentro = sum(resultados)
#pi_estimado = 4 * total_dentro / 2_000_000
#t2 = perf_counter()
#print(f"valor de π estimado: {pi_estimado}")
#print(f"tiempo con map: {t2-t1} segundos")
#print()


#t1 = perf_counter()
#pool = Pool(2)
#resultados = pool.map(cuantos_dentro, [1_000_000, 1_000_000])
#total_dentro = sum(resultados)
#pi_estimado = 4 * total_dentro / 2_000_000
#t2 = perf_counter()
#print(f"valor de π estimado: {pi_estimado}")
#print(f"tiempo con dos procesos: {t2-t1} segundos")
#print()
