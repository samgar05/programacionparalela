from threading import Thread, Lock
import time

class Bakery:
    def __init__(self, num_threads):
        # Initialize the Bakery algorithm with the given number of threads
        self.num_threads = num_threads
        # Each thread gets a ticket and choosing flag
        self.ticket = [0] * num_threads
        self.choosing = [False] * num_threads
        # Lock for thread synchronization
        self.lock = Lock()

    def lock_acquire(self, thread_id):
        # Acquire the lock using the Bakery algorithm
        self.choosing[thread_id] = True
        self.ticket[thread_id] = max(self.ticket) + 1
        self.choosing[thread_id] = False
        for i in range(self.num_threads):
            while self.choosing[i]:
                pass
            while (self.ticket[i] != 0 and (self.ticket[i], i) < (self.ticket[thread_id], thread_id)):
                pass

    def lock_release(self, thread_id):
        # Release the lock
        self.ticket[thread_id] = 0

class Counter:
    def __init__(self, bakery):
        # Initialize the counter with the Bakery algorithm instance
        self.bakery = bakery
        self.count = 0

    def increment(self, thread_id):
        # Increment the counter while ensuring mutual exclusion using Bakery algorithm
        self.bakery.lock_acquire(thread_id)
        self.count += 1
        print(f"Thread {thread_id}: Incremented count to {self.count}")
        self.bakery.lock_release(thread_id)

def test_bakery():
    num_threads = 4
    # Create a Bakery algorithm instance with the specified number of threads
    bakery = Bakery(num_threads)
    # Create a counter instance with the Bakery algorithm instance
    counter = Counter(bakery)
    threads = []

    # Create threads, each will increment the counter
    for i in range(num_threads):
        thread = Thread(target=counter.increment, args=(i,))
        threads.append(thread)

    # Start all threads
    for thread in threads:
        thread.start()

    # Wait for all threads to finish
    for thread in threads:
        thread.join()

if __name__ == "__main__":
    # Run the test function
    test_bakery()
