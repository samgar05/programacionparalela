
from multiprocessing import Lock
from multiprocessing import Semaphore


class ContadorSeguro:
    def __init__(self, num_hebras:int, inicial: int) -> None:
        self.__contador = inicial #valor inicial del Contador
        self.__num_hebras = in
        self.__lock = Lock()
        
    def incrementar(self) -> None:
        self.__lock.acquire()
        self.__contador += 1
        self.__lock.release()

    def decrementar(self) -> None:
        self.__lock.acquire()
        self.__contador -= 1
        self.__lock.release()
        
    def valor(self) -> int:
        self.__lock.acquire()
        v = self.__contador
        self.__lock.release()
        return v