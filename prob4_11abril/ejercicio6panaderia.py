from threading import Thread
from contador_seguro import ContadorSeguro




if __name__ == '__main__':
    
    num_hebras = 4

    cogiendo_numero = [False for _ in range(num_hebras)]
    numero = [0 for _ in range(num_hebras)]
    
    cs = ContadorSeguro(0)
    hebras = [Thread(target=cs.incrementar,  args=(i,)) for i in range(num_hebras)]
    
    for h in hebras:
        h.start()
    for h in hebras:
        h.join()
    print(cs.valor(0))