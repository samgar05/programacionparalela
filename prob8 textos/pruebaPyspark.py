
from pyspark.sql import SparkSession
spark = SparkSession.builder.master("local[*]").getOrCreate()
sc = spark.sparkContext

textRDD = sc.textFile("quijote.txt")


text2RDD = sc.textFile("fortunatayjacinta.txt")



sumar = lambda a, b: a + b
cortar_linea = lambda linea: linea.split()
tag_word = lambda palabra: (palabra,1)

rdd = sc.textFile("test/test1.txt") \
    .flatMap(cortar_linea) \
    .map(tag_word) \
    .reduceByKey(sumar)
