'''
Uso de threads para explorar su funcionamiento
'''

import threading
import time
import urllib.request
from sys import exit

# Función para descargar un archivo desde una URL
def download_file(url, filename):
    try:
        with urllib.request.urlopen(url) as response:
            data = response.read()
            with open(filename, 'wb') as fl:
                fl.write(data)
    except Exception as e:
        print(f"Failed to download file. Error: {e}")

# Función para crear un archivo con el nombre dado
def create_file(filename):
    try:
        fp = open(filename, 'w')
        fp.close()
        print(f"Archivo {filename} creado correctamente.")
    except Exception as e:
        print(f"Failed to create file. Error: {e}")

# Función para esperar la cantidad de segundos dada
def wait_seconds(seconds):
    time.sleep(seconds)
    print(f"Hebra esperó {seconds} segundos y ha finalizado.")

# Función principal para manejar las órdenes
def handle_order(order):
    # Verificamos el tipo de orden y creamos una hebra correspondiente
    if order.startswith("descarga"):
        url = order.split()[1]
        threading.Thread(target=download_file, args=(url, f"file_{time.time()}.jpg")).start()
    elif order.startswith("crea"):
        filename = order.split()[1]
        threading.Thread(target=create_file, args=(filename,)).start()
    elif order.startswith("espera"):
        seconds = int(order.split()[1])
        threading.Thread(target=wait_seconds, args=(seconds,)).start()
    elif order == "salir":
        print("Programa finalizado.")
        exit()
    else:
        print("Orden no reconocida.")

# Lógica principal del programa
MAX_THREADS = 10  # Número máximo de hebras activas permitidas
active_threads = []

files = ['https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/GIMS_2024%2C_Le_Grand-Saconnex_%28GIMS0007-4%29.jpg/120px-GIMS_2024%2C_Le_Grand-Saconnex_%28GIMS0007-4%29',
         'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/SPORTHOTEL_BOHEMIA_v_Rokytnice_nad_Jizerou_%2829.02.2024%29_4.jpg/1024px-SPORTHOTEL_BOHEMIA_v_Rokytnice_nad_Jizerou_%2829.02.2024%29_4',
         'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/H.W._Sumners%2C_Texas_LCCN2014687599_%28cropped%29.jpg/800px-H.W._Sumners%2C_Texas_LCCN2014687599_%28cropped%29',
         ]

while True:
    # Verificamos si hay espacio para más hebras
    if threading.active_count() - 1 < MAX_THREADS:
        order = input("Introduce una orden: ")
        if order:
            # Creamos una hebra para manejar la orden
            thread = threading.Thread(target=handle_order, args=(order,))
            thread.start()
            active_threads.append(thread)
    else:
        print("Número máximo de hebras activas alcanzado. Espera a que algunas terminen.")

    # Limpiamos las hebras finalizadas de la lista de hebras activas
    active_threads = [thread for thread in active_threads if thread.is_alive()]
    time.sleep(1)  # Esperamos un segundo antes de verificar de nuevo las órdenes
