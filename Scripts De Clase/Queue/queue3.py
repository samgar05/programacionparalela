from multiprocessing import Process, Queue


def trabajador(input_q: Queue, output_q: Queue) -> None:
    while not input_q.empty():
        texto = input_q.get()
        texto_procesado = texto[::-1]
        output_q.put(texto_procesado)


def main() -> None:
    arboles = ["abeto", "baobab", "cedro", "drago", "enebro"]
    input_q = Queue()
    output_q = Queue()

    for a in arboles:
        input_q.put(a)

    trabajadores = []
    for _ in range(3):
        p = Process(target=trabajador, args=(input_q, output_q))
        trabajadores.append(p)
        p.start()

    for p in trabajadores:
        p.join()

    while not output_q.empty():
        texto_procesado = output_q.get()
        print(f"texto procesado: {texto_procesado}", flush=True)


if __name__ == "__main__":
    main()
