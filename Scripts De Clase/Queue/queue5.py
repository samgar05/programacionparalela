from random import randint
from multiprocessing import Process, Queue


def enviar(q: Queue) -> None:
    cant = randint(1, 10)
    for _ in range(cant):
        dato = randint(1, 100)
        q.put(dato)
    q.put(None)


def recibir(q: Queue) -> None:
    x = q.get()
    while x is not None:
        print(x)
        x = q.get()


if __name__ == "__main__":
    q = Queue()
    p1 = Process(target=enviar, args=(q,))
    p2 = Process(target=recibir, args=(q,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
