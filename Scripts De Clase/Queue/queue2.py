from multiprocessing import Queue, Process, current_process


def trabajador(q: Queue) -> None:
    nombre = current_process().name
    dato = q.get()
    print(f'{nombre} ha recibido {dato}', flush=True)


if __name__ == '__main__':
    q = Queue()
    q.put("abeto")
    q.put("baobab")
    q.put("cedro")
    q.put("drago")

    procesos = [Process(target=trabajador, args=(q,)) for _ in range(4)]
    for p in procesos:
        p.start()
    for p in procesos:
        p.join()
