from multiprocessing import Process, Queue


def trabajador(input_q: Queue, output_q: Queue) -> None:
    while not input_q.empty():
        indice, texto = input_q.get()
        texto_procesado = texto[::-1]  # da la vuelta al texto
        output_q.put((indice, texto_procesado))


def main() -> None:
    texto = "abeto baobab cedro drago enebro"
    palabras = texto.split()
    input_q = Queue()
    output_q = Queue()
    for indice, texto in enumerate(palabras):
        input_q.put((indice, texto))

    trabajadores = []
    for _ in range(3):
        p = Process(target=trabajador, args=(input_q, output_q))
        trabajadores.append(p)
        p.start()

    for p in trabajadores:
        p.join()

    textos_res = []
    while not output_q.empty():
        textos_res.append(output_q.get())

    textos_res.sort(key=lambda x: x[0], reverse=True)
    res = " ".join([texto for _, texto in textos_res])
    print(f"texto procesado: {res}")


if __name__ == "__main__":
    main()
