from multiprocessing import Process, Queue
import random
import time


def productor(q: Queue, nr_datos: int) -> None:
    for _ in range(nr_datos):
        dato = random.randint(1, 100)
        print(f"producido {dato}")
        q.put(dato)
        time.sleep(random.random())
    print("un productor ha acabado")


def consumidor(q: Queue) -> None:
    while True:
        dato = q.get()
        if dato is None:
            break
        print(f"consumido {dato}")
    print("un consumidor ha acabado")


if __name__ == "__main__":
    nr_prod = 2
    nr_cons = 2
    nr_datos_por_prod = 5
    queue = Queue()

    prods = [Process(target=productor, args=(queue, nr_datos_por_prod)) for _ in range(nr_prod)]
    for p in prods:
        p.start()

    conss = [Process(target=consumidor, args=(queue,)) for _ in range(nr_cons)]
    for c in conss:
        c.start()

    for p in prods:
        p.join()
    for _ in range(nr_cons):
        queue.put(None)
    for c in conss:
        c.join()
