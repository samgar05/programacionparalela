from multiprocessing import Process, Queue


def enviar(q: Queue) -> None:
    q.put(33)


def recibir(q: Queue) -> None:
    x = q.get()
    print(x)


if __name__ == "__main__":
    q = Queue()
    p1 = Process(target=enviar, args=(q,))
    p2 = Process(target=recibir, args=(q,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
