from multiprocessing import Process


def algo() -> None:
    print("soy algo", flush=True)


def algo_mas(x: int) -> None:
    print(f"soy algo_mas y he recibido {x}", flush=True)


if __name__ == "__main__":
    p1 = Process(target=algo)
    p2 = Process(target=algo_mas, args=(2,))

    p1.start()
    p2.start()
    p1.join()
    p2.join()
