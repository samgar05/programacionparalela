from multiprocessing import Process


def cuadrado(n: int) -> None:
    cuad = n * n
    print(f"el cuadrado de {n} es {cuad}", flush=True)


if __name__ == "__main__":
    procesos = [Process(target=cuadrado, args=(i,)) for i in range(10)]
    for p in procesos:
        p.start()
    for p in procesos:
        p.join()
