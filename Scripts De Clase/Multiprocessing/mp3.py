from multiprocessing import Process


def nieto() -> None:
    print("soy un nieto", flush=True)


def hijo() -> None:
    print("soy un hijo", flush=True)
    proc_nietos = [Process(target=nieto) for _ in range(2)]
    for pn in proc_nietos:
        pn.start()
    for pn in proc_nietos:
        pn.join()


def padre() -> None:
    print(f"soy el padre", flush=True)
    proc_hijo = Process(target=hijo)
    proc_hijo.start()
    proc_hijo.join()


if __name__ == "__main__":
    padre()
