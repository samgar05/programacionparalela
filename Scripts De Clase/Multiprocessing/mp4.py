from multiprocessing import Process


x = 1


def func33() -> None:
    global x
    x = 33
    print(f'en func33 x vale {x}', flush=True)


def func55() -> None:
    global x
    x = 55
    print(f'en func55 x vale {x}', flush=True)


if __name__ == "__main__":
    p33 = Process(target=func33)
    p55 = Process(target=func55)
    p33.start()
    p55.start()
    p33.join()
    p55.join()
    print(f'en el proceso principal x vale {x}', flush=True)

    # func33()
    # print(f'ahora, en el proceso principal, x vale {x}', flush=True)
